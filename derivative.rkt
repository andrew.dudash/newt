#lang racket

; symbol calc

(module+ test
  (require rackunit)
  (check-equal? (power-simplify '(expt x 1)) 'x)
  (check-equal? (power-simplify '(expt x 0)) 1)
  (check-equal? (power-simplify '(expt x 10)) '(expt x 10))
  (check-equal? (power-simplify '(expt 2 3)) 8))

(define (power-simplify expr)
  (match expr
    [(list 'expt x 1) x]
    [(list 'expt 'x 0) 1]
    [(list 'expt (and (? number?) x) (and (? number?) k)) (expt x k)]
    [else expr]))

(module+ test
  (check-equal? (power-rule '(expt x 4)) '(* 4 (expt x 3)))
  (check-equal? (power-rule '(expt x 0)) 0)
  (check-equal? (power-rule '(expt x 1)) 1))

(define (power-rule expr)
  (match expr
    [(list 'expt 'x (and (? number?) k))
     (product-simplify (list '* k (power-simplify (list 'expt 'x (sub1 k)))))]
    [(list 'expt u (and (? number?) k))
     (product-simplify (list '* k (power-simplify (list 'expt u (sub1 k))) (derivative u)))]))

(define (product lst)
  (foldl * 1 lst))

(module+ test
  (check-equal? (product-simplify '(* x 4 5)) '(* 20 x))
  (check-equal? (product-simplify '(* x)) 'x)
  (check-equal? (product-simplify '(* x 1)) 'x)
  (check-equal? (product-simplify '(* 5 1/5 x)) 'x)
  (check-equal? (product-simplify '(* 0 x)) 0)
  (check-equal? (product-simplify '(* 5 3)) 15))

(define (product-simplify expr)
  (define-values (constants non-constants) (partition number? (drop expr 1)))
  (define constant (product constants))
  (cond [(null? non-constants)
         constant]
        [(and (= 1 constant) (= (length non-constants) 1))
         (first non-constants)]
        [(= 1 constant)
         `(* ,@non-constants)]
        [(= 0 constant)
         0]
        [else
         `(* ,constant ,@non-constants)]))

(define (sum lst)
  (foldl + 0 lst))

(module+ test
  (check-equal? (sum-simplify '(+ 1 2 3)) 6)
  (check-equal? (sum-simplify '(+ 0 (expt x 2))) '(expt x 2))
  (check-equal? (sum-simplify '(+ 1 -1 (expt x 2))) '(expt x 2)))

(define (sum-simplify expr)
  (define-values (constants non-constants) (partition number? (drop expr 1)))
  (define constant (sum constants))
  (cond [(null? non-constants)
         constant]
        [(and (zero? constant) (= (length non-constants) 1))
         (first non-constants)]
        [(zero? constant)
         `(+ ,@non-constants)]
        [else `(+ ,constant ,@non-constants)]))

(module+ test
  (check-equal? (sum-rule '(+ 3 4 5)) 0)
  (check-equal? (sum-rule '(+ x x x)) 3)
  (check-equal? (sum-rule '(+ 2 (expt x 3))) '(* 3 (expt x 2))))

(define (sum-rule expr)
  (sum-simplify `(+ ,@(map derivative (drop expr 1)))))

(module+ test
  (check-equal? (product-rule '(* 3 x)) 3)
  (check-equal? (product-rule '(* (expt x 2) (expt x 3))) '(* 5 (expt x 4))))

(define (product-rule expr)
  (


(module+ test
  (check-equal? (derivative '(+ 10 x)) 1)
  (check-equal? (derivative '(expt x 1)) 1)
  (check-equal? (derivative '(expt x 3)) '(* 3 (expt x 2)))
  (check-equal? (derivative '(expt x 2)) '(* 2 x))
  (check-equal? (derivative '(+ (expt x 2) (expt x 1) 10)) '(+ 1 (* 2 x))))

(define (derivative expr)
  (if (list? expr)
      (derivative-pair expr)
      (derivative-atom expr)))

(define (derivative-pair expr)
  (match (first expr)
    ['expt (power-rule expr)]
    ['+ (sum-rule expr)]))

(define (derivative-atom expr)
  (match expr
    [(? number?) 0]
    ['x 1]))
    

